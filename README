
Pg  --  a libpq-based PostgreSQL interface for Perl

# $Id: README,v 1.17 2004/04/21 17:41:14 bmomjian Exp $

DESCRIPTION:
------------

This is version 2.1.0 of Pg (previously called pgsql_perl5 and pgperl). 
The web site for this interface is at:

	http://gborg.postgresql.org/project/pgperl/projdisplay.php

For information about PostgreSQL, visit:
    
	http://www.postgresql.org/

Pg is an interface between Larry Wall's language perl version 5 and 
the database PostgreSQL (previously Postgres95). This has been done by using 
the Perl5 application programming interface for C extensions which calls the 
Postgres programmer's interface LIBPQ. 


COPYRIGHT:
----------

	Copyright (c) 1997, 1998  Edmund Mergl
	Copyright (c) 1999-2002 PostgreSQL Global Development Group

You may distribute under the terms of either the GNU General Public
License or the Artistic License, as specified in the Perl README file.



IF YOU HAVE PROBLEMS:
---------------------

Please send comments and bug-reports to <pgperl-general@gborg.postgresql.org>.

Please include the output of:
	o perl -v
	o perl -V
	o the PostgreSQL version
	o version of Pg

in your bug-report.


REQUIREMENTS:
-------------

        build, test and install Perl5          (at least 5.003)
        build, test and install PostgreSQL     (at least 6.5)


PLATFORMS:
----------

  This release of Pg has been developed using Linux 2.2 with 
  dynamic loading for the perl extensions. Let me know, if there are 
  any problems with other platforms. 


INSTALLATION:
-------------

The Makefile uses the environment variables POSTGRES_INCLUDE and
POSTGRES_LIB, or POSTGRES_HOME to find the library libpq.so and 
the include file libpq-fe.h. 

If you are using pre-compiled binaries for postgresql and you 
can't find libpq.so and libpq-fe.h most probably you forgot to
install the additional development-package for postgresql. 

	1.   POSTGRES_HOME=/usr/local/pgsql; export POSTGRES_HOME
	2.   perl Makefile.PL
	3.   make
	4.   PGDATABASE=test;  export PGDATABASE
	     PGUSER=postgres;  export PGUSER
	5.   make test
	6.   make install

Do steps 1 - 5 as normal user, not as root!


TESTING:
--------

Run 'make test'.

Note, that the user running this script must have been created with the
access rights to create databases *AND* users!  Do not run this script
as root!

If testing fails with the message 'login failed', please check if access
to the database template1 as well as pgperltest is not protected via
pg_hba.conf. 

If you are using the shared library libpq.so check if your dynamic
loader finds libpq.so. With Linux the command /sbin/ldconfig -v should
tell you,  where it finds libpq.so. If ldconfig does not find libpq.so,
either add an  appropriate entry to /etc/ld.so.conf and re-run ldconfig
or add the path to  the environment variable LD_LIBRARY_PATH.  A typical
error message resulting from not finding libpq.so is: 

  Can't load './blib/arch/auto/Pg/Pg.so' for module Pg: File not found at

Some linux distributions have an incomplete perl installation.  If you
have compile errors like "XS_VERSION_BOOTCHECK undeclared", run: 

          find <path_to_perl>/lib/ -name XSUB.h -print

If this file is not present, you need to recompile and reinstall perl. 

Also RedHat 5.0 seems to have an incomplete perl-installation: if  you
get error message during the installation complaining about a  missing
perllocal.pod, you need to recompile and reinstall perl. 

SGI users: if you get segmentation faults make sure, you use the malloc which 
           comes with perl when compiling perl (the default is not to).
           "David R. Noble" <drnoble@engsci.sandia.gov>

HP users: if you get error messages like:

              can't open shared library: .../lib/libpq.sl
              No such file or directory

          when running the test script, try to replace the
          'shared' option in the LDDFLAGS with 'archive'.
          Dan Lauterbach <danla@dimensional.com>


DOCUMENTATION:
--------------

Detailed documentation can be found in Pg.pm. Use 'perldoc Pg' after
installation to read the documentation. 

Original author Edmund Mergl <E.Mergl@bawue.de>.
